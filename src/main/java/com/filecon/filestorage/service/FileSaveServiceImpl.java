package com.filecon.filestorage.service;

import com.filecon.filestorage.document.ConvertedFile;
import com.filecon.filestorage.document.RawFile;
import com.filecon.filestorage.dto.ConvertedFileDataDto;
import com.filecon.filestorage.dto.FileDto;
import com.filecon.filestorage.repository.ConvertedFileRepository;
import com.filecon.filestorage.repository.RawFileRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@Service
@Slf4j
public class FileSaveServiceImpl implements FileSaveService {

    private final RawFileRepository rawFileRepository;
    private final ConvertedFileRepository convertedFileRepository;

    public FileSaveServiceImpl(RawFileRepository rawFileRepository,
                               ConvertedFileRepository convertedFileRepository) {
        this.rawFileRepository = rawFileRepository;
        this.convertedFileRepository = convertedFileRepository;
    }

    @Override
    public String saveRawFile(MultipartFile file, Long userId) {
        String originalFileName = file.getOriginalFilename();
        log.info("saving original file {}", originalFileName);
        RawFile rawFile = new RawFile();
        rawFile.setFileName(originalFileName);
        rawFile.setUserId(userId);
        rawFile.setFileSize(file.getSize());
        rawFile.setContentType(file.getContentType());
        try {
            rawFile.setFileContent(file.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException(e.getMessage());
        }
        RawFile savedFile = rawFileRepository.save(rawFile);
        log.info("saved original file {} by id {}", originalFileName, savedFile.getId());
        return savedFile.getId();
    }


    @Override
    public String saveConvertedFile(MultipartFile file, ConvertedFileDataDto convertedFileDataDto) {
        String originalFileName = file.getOriginalFilename();
        log.info("saving converted file {}", originalFileName);
        ConvertedFile convertedFile = new ConvertedFile();
        convertedFile.setFileName(originalFileName);
        convertedFile.setRawFileId(convertedFileDataDto.getRawFileId());
        //convertedFile.setUserId(convertedFileDataDto.getUserId());
        convertedFile.setFileSize(file.getSize());
        convertedFile.setContentType(file.getContentType());
        try {
            convertedFile.setFileContent(file.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException(e.getMessage());
        }
        ConvertedFile savedFile = convertedFileRepository.save(convertedFile);
        log.info("saved converted file {} by id {}", originalFileName, savedFile.getId());
        return savedFile.getId();
    }

    @Override
    public FileDto getConvertedFileByRawFileId(String rawFileId) {
        ConvertedFile convertedFile = convertedFileRepository.getFirstByRawFileIdOrderByIdDesc(rawFileId);
        FileDto fileDto = new FileDto();
        fileDto.setBytes(convertedFile.getFileContent());
        fileDto.setContentType(convertedFile.getContentType());
        fileDto.setFileName(convertedFile.getFileName());
        fileDto.setFileSize(convertedFile.getFileSize());
        //fileDto.setUserId(convertedFile.getUserId());
        return fileDto;
    }
}
