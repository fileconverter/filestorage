package com.filecon.filestorage.service;

import com.filecon.filestorage.dto.ConvertedFileDataDto;
import com.filecon.filestorage.dto.FileDto;
import org.springframework.web.multipart.MultipartFile;

public interface FileSaveService {

    String saveRawFile(MultipartFile file, Long userId);

    String saveConvertedFile(MultipartFile file, ConvertedFileDataDto convertedFileDataDto);

    FileDto getConvertedFileByRawFileId(String rawFileId);

}
