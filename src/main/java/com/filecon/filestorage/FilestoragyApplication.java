package com.filecon.filestorage;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@SpringBootApplication(exclude = SecurityAutoConfiguration.class)
@EnableEurekaClient
@EnableMongoRepositories(basePackages = "com.filecon.filestorage.repository")
public class FilestoragyApplication {

    public static void main(String[] args) {
        SpringApplication.run(FilestoragyApplication.class, args);
    }

}
