package com.filecon.filestorage.document;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Getter
@Setter
@NoArgsConstructor
@Document(collection = "converted_file")
public class ConvertedFile {

    @Id
    private String id;

    @Field("fileName")
    private String fileName;

    @Field("size")
    private Long fileSize;

    @Field("contentType")
    private String contentType;

//    @Field("userId")
//    @Indexed
//    private Long userId;

    @Field("fileContent")
    private byte[] fileContent;

    @Field("rawFileId")
    @Indexed
    private Long rawFileId;
}
