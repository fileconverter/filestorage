package com.filecon.filestorage.repository;

import com.filecon.filestorage.document.RawFile;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface RawFileRepository extends MongoRepository<RawFile, Long> {
}
