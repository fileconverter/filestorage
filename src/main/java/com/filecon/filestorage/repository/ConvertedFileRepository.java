package com.filecon.filestorage.repository;

import com.filecon.filestorage.document.ConvertedFile;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface ConvertedFileRepository extends MongoRepository<ConvertedFile, String> {

    ConvertedFile getFirstByRawFileIdOrderByIdDesc(String rawFileId);
}
