package com.filecon.filestorage.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.filecon.filestorage.dto.ConvertedFileDataDto;
import com.filecon.filestorage.dto.FileDto;
import com.filecon.filestorage.service.FileSaveService;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.websocket.server.PathParam;
import java.io.IOException;

@Controller
@RequestMapping("file-storage")
public class FileController {

    private final FileSaveService fileSaveService;
    private final ObjectMapper objectMapper;

    public FileController(FileSaveService fileSaveService, ObjectMapper objectMapper) {
        this.fileSaveService = fileSaveService;
        this.objectMapper = objectMapper;
    }

    @PostMapping(path = "/raw/save")
    @ResponseBody
    public String saveRawFile(@RequestParam("file") MultipartFile file,
                            @RequestParam("userId") Long userId) {
        String id = fileSaveService.saveRawFile(file, userId);
        return id;
    }

    @PostMapping(path = "/converted/save")
    @ResponseBody
    public String saveConvertedFile(@RequestParam("file") MultipartFile file,
                                                 @RequestParam("fileInfo") String fileInfo) throws IOException {
        ConvertedFileDataDto convertedFileDataDto = objectMapper.readValue(fileInfo, ConvertedFileDataDto.class);
        String id = fileSaveService.saveConvertedFile(file, convertedFileDataDto);
        return id;
    }

    @GetMapping(path = "/converted/{rawFileId}")
    public HttpEntity<byte[]> getConvertedFileByRawFileId(@PathParam("rawFileId") String rawFileId) {
        FileDto fileDto = fileSaveService.getConvertedFileByRawFileId(rawFileId);
        HttpHeaders header = new HttpHeaders();
        header.set("charset", "utf-8");
        //header.setContentType("application/force-download");
        header.set(HttpHeaders.CONTENT_TYPE, fileDto.getContentType() + "; charset=utf-8");
        header.setContentDispositionFormData("attachment", fileDto.getFileName());
        header.setContentLength(fileDto.getFileSize());
        return new HttpEntity<byte[]>(fileDto.getBytes(), header);
    }

}
