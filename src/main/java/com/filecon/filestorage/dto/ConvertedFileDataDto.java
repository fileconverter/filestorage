package com.filecon.filestorage.dto;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class ConvertedFileDataDto {

    private Long userId;
    private Long rawFileId;
}
