package com.filecon.filestorage.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class FileDto {

    private String fileName;
    private Long fileSize;
    private String contentType;
    private Long userId;
    private byte[] bytes;
}
